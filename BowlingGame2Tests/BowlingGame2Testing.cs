﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BowlingGame2;

namespace BowlingGame2Tests
{
    [TestClass]
    public class BowlingGame2Testing
    {


        private BowlingGame game = new BowlingGame();

        [TestMethod]
        public void GutterGameScore0()
        {
            //arrange
            //act
            RollMany(20, 0);

            //assert
            Assert.AreEqual(0, game.Score());
        }


        [TestMethod]
        public void All_1s_Score_20()
        {
            //arrange
            //act
            RollMany(20, 1);
            
            //assert
            Assert.AreEqual(20, game.Score());
        }



        [TestMethod]
        public void Spare_then_3pins_then_0s_Score_16()
        {
            //arrange
            //act
            RollSpare();
            game.Roll(3);
            RollMany(17, 0);

            //assert
            Assert.AreEqual(16, game.Score());
        }


        
        [TestMethod]
        public void Strike_then_3pins_then_4pins_then_0s_Score_24()
        {
            //arrange
            //act
            RollStrike();
            game.Roll(3);
            game.Roll(4);

            RollMany(16, 0);

            //assert
            Assert.AreEqual(24, game.Score());
        }

        [TestMethod]
        public void All_Strikes_Score_300()
        {
            //arrange
            //act
            RollMany(12, 10);

            //assert
            Assert.AreEqual(300, game.Score());
        }







        private void RollStrike()
        {
            game.Roll(10);
        }

        private void RollSpare()
        {
            game.Roll(2);
            game.Roll(8);
        }

        private void RollMany(int rolls, int pins)
        {
            for (int i = 0; i < rolls; i++)
            {
                game.Roll(pins);
            }
        }




    }
}
