﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculator2
{
    public class StringCalculator
    {
        private int sum = 0;

        public int Add(string stringOfNumbers)
        {
            
            if (stringOfNumbers != "") //if string isn't empty, start adding up the numbers
            {
                if (stringOfNumbers.StartsWith("//")) //user supplied a delimiter
                {
                    //take off the //
                    stringOfNumbers = stringOfNumbers.Substring(2);
                    //then capture the delimeter
                    string userDelimiter = stringOfNumbers.Substring(0, 1);
                    //then take out the delimiter and newline
                    stringOfNumbers = stringOfNumbers.Substring(2);

                    //with remaining string, "split and add" using the users delimiter
                    char[] userDelimiterCharArray = userDelimiter.ToCharArray();

                    SplitAndAdd(userDelimiterCharArray, stringOfNumbers);
                }

                else //user did not supply a delimiter
                {
                    //split the string by comma or newline because could be multiple numbers in the string
                    char[] delimiter = { ',', '\n' };

                    SplitAndAdd(delimiter, stringOfNumbers);

                                      
                }
            }
            return sum;


        }

        //add numbers in string, given a delimiter
        private void SplitAndAdd(char[] delimiter, string numbers)
        {
            string[] stringArray = numbers.Split(delimiter);

            //loop through array and add the numbrs together
            foreach (string item in stringArray)
            {
                sum += int.Parse(item);
            }

            
        }
    }
}
