﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandonsGame
{
    public class FizzBuzzGame
    {
        public String Result(int number)
        {
            //if divisible by 3 and 5, result is fizzbuzz
            if ((number % 3 == 0) && (number % 5 == 0))
            {
                return "fizzbuzz";
            }

            //if divisible by 3 only, then result is fizz
            if (number % 3 == 0)
            {
                return "fizz";
            }
            //if divisible by 5 only, then reslut is buzz
            if (number % 5 == 0)
            {
                return "buzz";
            }
            else //not fizz or buzz, just return the number as a string
            {
                string result = number.ToString();
                return result;
            }
            
            
            
        }
    }
}
