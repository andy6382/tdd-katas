﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LeapYears;


namespace LeapYearsTests
{
    [TestClass]
    public class LeapYearToolTests
    {
        [DataTestMethod]
        [DataRow(400)]
        [DataRow(800)]
        [DataRow(4000)]
        [DataRow(2400)]
        public void Year400isLeapYear(int year)
        {
            //arrange
            bool isLeapYear;

            //act
            isLeapYear = LeapYearTool.IsLeapYear(year);

            //assert
            Assert.IsTrue(isLeapYear);
        
        }


        [DataTestMethod]
        [DataRow(300)]
        [DataRow(200)]
        [DataRow(21000)]
        [DataRow(1900)]
        public void YearDivisbleBy_100_and_not_divisible_by_400_NOT_LeapYear(int year)
        {
            //arrange
            bool isLeapYear;

            //act
            isLeapYear = LeapYearTool.IsLeapYear(year);

            //assert
            Assert.IsFalse(isLeapYear);

        }



        [DataTestMethod]
        [DataRow(2004)]
        [DataRow(2040)]
        [DataRow(1944)]
        [DataRow(1776)]
        public void YearDivisbleBy_4_and_not_by_100_is_LeapYear(int year)
        {
            //arrange
            bool isLeapYear;

            //act
            isLeapYear = LeapYearTool.IsLeapYear(year);

            //assert
            Assert.IsTrue(isLeapYear);

        }


        [DataTestMethod]
        [DataRow(2001)]
        [DataRow(2018)]
        [DataRow(1982)]
        [DataRow(1985)]
        public void years_notDivisibleBy_4_not_leapYear(int year)
        {
            //arrange
            bool isLeapYear;

            //act
            isLeapYear = LeapYearTool.IsLeapYear(year);

            //assert
            Assert.IsFalse(isLeapYear);

        }

    }
}
