﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeapYears
{
    public class LeapYearTool
    {
        public static Boolean IsLeapYear(int year)
        {
            if (year % 400 == 0) //year divisible by 400 is a leap year
            {
                return true;
            }
            if (year % 4 == 0) //year divisible by 4 is a leap year
            {
                if (year % 100 == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
                
            }
            return false;
            
        }
    }
}
