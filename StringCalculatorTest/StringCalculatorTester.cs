﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringCalculator;

namespace StringCalculatorTest
{
    [TestClass]
    public class StringCalculatorTester
    {
        [TestMethod]
        public void StringCalculatorObjectExist()
        {
            //arrange
            StringCalculatorTool calc;

            //action
            calc = new StringCalculatorTool();

            //assert
            Assert.IsNotNull(calc);

        }


        [TestMethod]
        public void EmptyStringReturnsZero()
        {
            //arrange
            StringCalculatorTool calc = new StringCalculatorTool();

            //action
            int result = calc.calculate("");

            //assert
            Assert.AreEqual(0, result);

        }


        [DataTestMethod]
        [DataRow("1")]
        [DataRow("2")]
        [DataRow("99")]
        [DataRow("145")]
        public void SingleNumberStringReturnsValue(string data)
        {
            //arrange
            StringCalculatorTool calc = new StringCalculatorTool();
            String value = data;
            int TestResult = int.Parse(data);

            //action
            int result = calc.calculate(value);

            //assert
            Assert.AreEqual(TestResult, result);

        }


        [DataTestMethod]
        [DataRow("1, 2", 3)]
        [DataRow("2, 3, 5",10)]
        [DataRow("300, 5, 8",313)]
        public void CommaSeperatedNumbersReturnsSum(string nums, int delimSum)
        {
            //arrange
            StringCalculatorTool calc = new StringCalculatorTool();

            //action
            int result = calc.calculate(nums);

            //assert
            Assert.AreEqual(delimSum, result);

        }

        [TestMethod]
        public void TwoNumsNewLineSeparatedReturnSum()
        {
            //arrange
            StringCalculatorTool calc = new StringCalculatorTool();

            //action
            int result = calc.calculate("1" + Environment.NewLine + "2");

            //assert
            Assert.AreEqual(3, result);
        }

        [DataTestMethod]
        [DataRow("1\r\n2\r\n2", 5)]
        [DataRow("100\r\n200\r\n300", 600)]
        [DataRow("1\r\n1\r\n1", 3)]
        public void ThreeNumsNewLineSeparatedReturnSum(string nums, int delimsums)
        {
            //arrange
            StringCalculatorTool calc = new StringCalculatorTool();

            //action
            int result = calc.calculate(nums);

            //assert
            Assert.AreEqual(delimsums, result);

        }


        [TestMethod]
        public void ThreeNumbersDelimitedEitherWayReturnsSum()
        {
            //arrange
            StringCalculatorTool calc = new StringCalculatorTool();


            //act
            int result = calc.calculate("1,1" + Environment.NewLine + "1");


            //assert
            Assert.AreEqual(3, result);
        }

        [DataTestMethod]
        [DataRow("1\r\n2,2", 5)]
        [DataRow("1,2\r\n2", 5)]
        [DataRow("100\r\n200,29", 329)]
        public void AnyThreeNumbersBothDelimitedEitherWayReturnsSum(string nums, int expected)
        {
            //arrange
            StringCalculatorTool calc = new StringCalculatorTool();


            //act
            int result = calc.calculate(nums);


            //assert
            Assert.AreEqual(expected, result);
        }


        [TestMethod]
        public void SingleNegativeNumberThrowsException()
        {
            //arrange
            StringCalculatorTool calc = new StringCalculatorTool();

            //action
            Assert.ThrowsException<ArgumentException>(() => calc.calculate("-1"));


            //assert


        }

        [TestMethod]
        public void NegativeNumberReturnDelimThrowsException()
        {
            //arrange
            StringCalculatorTool calc = new StringCalculatorTool();

            //action
            Assert.ThrowsException<ArgumentException>(() => calc.calculate("-1" + Environment.NewLine + "2"));
           

            //assert
            

        }

        [TestMethod]
        public void NegativeNumberCommaDelimThrowsException()
        {
            //arrange
            StringCalculatorTool calc = new StringCalculatorTool();

            //action
            Assert.ThrowsException<ArgumentException>(() => calc.calculate("-1,2"));


            //assert


        }


        [TestMethod]
        public void Number1001Ignored()
        {
            //arrange
            StringCalculatorTool calc = new StringCalculatorTool();

            //action
            int result = calc.calculate("1001");


            //assert
            Assert.AreEqual(0, result);

        }


        //Testing inputs with different combinations of numbers over 1000
        [DataTestMethod]
        [DataRow("1001,2000",0)]
        [DataRow("1001,2000", 0)]
        [DataRow("1001\r\n2000", 0)]
        [DataRow("1001\r\n2000,3", 3)]
        public void Number1001IgnoredManyCombinations(string nums, int expected)
        {
            //arrange
            StringCalculatorTool calc = new StringCalculatorTool();

            //action
            int result = calc.calculate(nums);


            //assert
            Assert.AreEqual(expected, result);

        }
    }
}
