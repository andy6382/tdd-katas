﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGame2
{
    public class BowlingGame
    {

        private int totalScore = 0;

        List<int> pinsRolls = new List<int>();

        //logic that happens everytime a roll occurs
        //keep a running count of how many pins have been knocked down
        public void Roll(int pins)
        {
            pinsRolls.Add(pins);
            
        }

        //calculate the score at the end of the game after all the rolls have been done
        public int Score()
        {

            //for scoring spares, we must keep track of "frames"... to identify when a spare occurs
            //occurs when 2 rolls add up to 10.
            //we know there are 10 frames... so we will use that knowledge

            int roll = 0;
            for (int frame = 0; frame < 10; frame++)
            {
                //if strike
                if (pinsRolls[roll] == 10)
                {
                    totalScore += 10 + pinsRolls[roll + 1] + pinsRolls[roll + 2];
                    roll++;
                }
                else
                //if spare
                if (pinsRolls[roll] + pinsRolls[roll + 1] == 10)
                {
                    totalScore += 10 + pinsRolls[roll + 2];
                    roll += 2;
                } else
                {
                    totalScore += pinsRolls[roll] + pinsRolls[roll + 1];
                    roll += 2;
                }

                
            }

            return totalScore;
        }
    }
}
