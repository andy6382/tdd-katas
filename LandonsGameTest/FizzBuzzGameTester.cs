﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LandonsGame;

namespace LandonsGameTest
{
    [TestClass]
    public class FizzBuzzGameTester
    {

        FizzBuzzGame game = new FizzBuzzGame();
        string result;

        [TestMethod]
        public void FizzBuzzGameExist()
        {
            //Arrange

            //Action
            
            //Assert
            Assert.IsNotNull(game);

        }


        [TestMethod]
        public void HasResultMethod()
        {
            //Arrange
            
            //Action 
            
            //Assert
            Assert.IsNotNull(game.Result(3));
        }


        [TestMethod]
        public void Input_1_Result_1()
        {
            //Arrange
            
            //Action 
            result = game.Result(1);


            //Assert
            Assert.AreEqual("1", result);
        }


        [TestMethod]
        public void Input_2_Result_2()
        {
            //Arrange
            FizzBuzzGame game = new FizzBuzzGame();

            //Action 
            result = game.Result(2);


            //Assert
            Assert.AreEqual("2", result);
        }



        [TestMethod]
        public void Input_3_Result_fizz()
        {
            //Arrange
            
            //Action
            result = game.Result(3);


            //Assert
            Assert.AreEqual("fizz", result);
        }

        [TestMethod]
        public void Input_6_Result_fizz()
        {
            //Arrange

            //Action
            result = game.Result(6);


            //Assert
            Assert.AreEqual("fizz", result);
        }


        [DataTestMethod]
        [DataRow(3)]
        [DataRow(6)]
        [DataRow(9)]
        [DataRow(12)]
        [DataRow(99)]
        [DataRow(96)]
        [DataRow(93)]
        public void Input_DivisibleBy3_Result_fizz(int a)
        {
            //Arrange

            //Action
            result = game.Result(a);


            //Assert
            Assert.AreEqual("fizz", result);
        }


        [DataTestMethod]
        [DataRow(5)]
        [DataRow(10)]
        [DataRow(100)]
        public void Input_DivisibleBy5_Result_buzz(int a)
        {
            //Arrange

            //Action
            result = game.Result(a);


            //Assert
            Assert.AreEqual("buzz", result);
        }

        [DataTestMethod]
        [DataRow(15)]
        [DataRow(30)]
        [DataRow(45)]
        [DataRow(60)]
        public void Input_DivisibleBy5and3_Result_fizzbuzz(int a)
        {
            //Arrange

            //Action
            result = game.Result(a);


            //Assert
            Assert.AreEqual("fizzbuzz", result);
        }


    }
}
