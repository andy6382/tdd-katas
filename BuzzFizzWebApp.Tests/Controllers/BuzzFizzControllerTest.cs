﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BuzzFizzWebApp;
using BuzzFizzWebApp.Controllers;

namespace BuzzFizzWebApp.Tests.Controllers
{
    [TestClass]
    public class BuzzFizzControllerTest
    {

        [TestMethod]
        public void arg1result1()
        {
            // Arrange
            BuzzFizzController controller = new BuzzFizzController();

            // Act
            string result = controller.Get(1);

            // Assert
            Assert.AreEqual("1", result);
        }

        [TestMethod]
        public void arg2result2()
        {
            // Arrange
            BuzzFizzController controller = new BuzzFizzController();

            // Act
            string result = controller.Get(2);

            // Assert
            Assert.AreEqual("2", result);
        }

        [TestMethod]
        public void arg3resultBuzz()
        {
            // Arrange
            BuzzFizzController controller = new BuzzFizzController();

            // Act
            string result = controller.Get(3);

            // Assert
            Assert.AreEqual("Buzz", result);
        }

        [TestMethod]
        public void arg5resultFizz()
        {
            // Arrange
            BuzzFizzController controller = new BuzzFizzController();

            // Act
            string result = controller.Get(5);

            // Assert
            Assert.AreEqual("Fizz", result);
        }

        [DataTestMethod]
        [DataRow(3)]
        [DataRow(6)]
        [DataRow(9)]
        [DataRow(21)]
        [DataRow(99)]
        public void argMultOf3resultBuzz(int num)
        {
            // Arrange
            BuzzFizzController controller = new BuzzFizzController();

            // Act
            string result = controller.Get(num);

            // Assert
            Assert.AreEqual("Buzz", result);
        }


        [TestMethod]
        public void arg10resultFizz()
        {
            // Arrange
            BuzzFizzController controller = new BuzzFizzController();

            // Act
            string result = controller.Get(10);

            // Assert
            Assert.AreEqual("Fizz", result);
        }

        [TestMethod]
        public void arg15resultBuzzFizz()
        {
            // Arrange
            BuzzFizzController controller = new BuzzFizzController();

            // Act
            string result = controller.Get(15);

            // Assert
            Assert.AreEqual("BuzzFizz", result);
        }

        [DataTestMethod]
        [DataRow(15)]
        [DataRow(30)]
        [DataRow(45)]
        [DataRow(60)]
        [DataRow(75)]
        public void argDivBy3AndDivBy5resultBuzzFizz(int num)
        {
            // Arrange
            BuzzFizzController controller = new BuzzFizzController();

            // Act
            string result = controller.Get(num);

            // Assert
            Assert.AreEqual("BuzzFizz", result);
        }

    }
}
