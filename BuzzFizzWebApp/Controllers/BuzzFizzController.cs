﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BuzzFizzWebApp.Controllers
{
    public class BuzzFizzController : ApiController
    {
        
        /// <summary>
        /// Will let user know if the number provided is buzz, fizz, or buzzfizz.
        /// Numbers divisible by 3 are "buzz", divisible by 5 are "fizz" and divisible
        /// by both 3 and 5 are "buzzfizz"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/BuzzFizz/5 
        public string Get(int id)
        {
            if ((id % 3 == 0) && (id % 5 == 0)) return "BuzzFizz";

            if (id % 3 == 0) return "Buzz";

            if (id % 5 == 0) return "Fizz";

            return id.ToString();
        }

        

        

        
    }
}
