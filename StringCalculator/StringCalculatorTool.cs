﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculator
{
    public class StringCalculatorTool
    {

        //add all the numbers in a string and return an integer
        //numbers can't be negative, and can be delimitted by newline, or comma
        public int calculate(string v)
        {
            
            
            //if empty string, return zero
            if (v == "")
            {
                return 0;
            }

            //otherwise splice string into numbers and add them
            return SpliceAdder(v, "," + Environment.NewLine);
        }




        //method for to splice a string by a substring, and add all the items
        private int SpliceAdder(string nums, string splicer)
        {
            int total = 0;
            char[] splicerCharArray = splicer.ToCharArray();
            String[] stringArray = nums.Split(splicerCharArray);

            //add up all the numbers
            foreach (String item in stringArray)
            {
                //sometimes the newline delimiter causes a blank item in the string array, ignore this
                if (item != "")
                {
                    //Throw exception if item is negative number (starts with -)
                    if (item.StartsWith("-"))
                    {
                        throw new System.ArgumentException("Detected a negative (-) sign, negative numbers not allowed");
                    }
                    //turn item into an integer.
                    int itemAsInt = int.Parse(item);

                    //ignore item if it is greater than 1000.
                    if (itemAsInt <= 1000)
                    {
                        total += itemAsInt;
                    }
                    
                }
            }
            return total;

        }





    }
}
