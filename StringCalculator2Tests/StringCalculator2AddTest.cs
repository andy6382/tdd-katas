﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringCalculator2;

namespace StringCalculator2Tests
{
    [TestClass]
    public class StringCalculator2AddTest
    {

        StringCalculator calc = new StringCalculator();
        int sum;

        [TestMethod]
        public void AddMethodExists()
        {
            //arrange

            //act
            sum = calc.Add("1");

            //assert
            Assert.IsNotNull(sum);
        }


        [TestMethod]
        public void AddMethod_TakeEmptyStringReturn0()
        {
            //arrange
            

            //act
            sum = calc.Add("");

            //assert
            Assert.AreEqual(0,sum);
        }

        [DataTestMethod]
        [DataRow("1", 1)]
        [DataRow("2", 2)]
        public void AddMethod_Take1numberReturnThatNumber(string number, int realSum)
        {
            //arrange
            
            //act
            sum = calc.Add(number);

            //assert
            Assert.AreEqual(realSum, sum);
        }

        [DataTestMethod]
        [DataRow("1,2", 3)]
        [DataRow("2,10", 12)]
        public void AddMethod_Take2numbersReturnTheSum(string numbers, int realSum)
        {
            //arrange
                        
            //act
            sum = calc.Add(numbers);

            //assert
            Assert.AreEqual(realSum, sum);
        }

        [DataTestMethod]
        [DataRow("1,2,1", 4)]
        [DataRow("2,10,2", 14)]
        public void AddMethod_Take3numbersReturnTheSum(string numbers, int realSum)
        {
            //arrange
            
            //act
            sum = calc.Add(numbers);

            //assert
            Assert.AreEqual(realSum, sum);
        }

        [DataTestMethod]
        [DataRow("1,2,1,6", 10)]
        [DataRow("2,10,2,50,600", 664)]
        public void AddMethod_TakesManynumbersReturnTheSum(string numbers, int realSum)
        {
            //arrange

            //act
            sum = calc.Add(numbers);

            //assert
            Assert.AreEqual(realSum, sum);
        }


        [DataTestMethod]
        [DataRow("1\n2,2", 5)]
        [DataRow("2\n10", 12)]
        [DataRow("1,2\n2", 5)]
        public void AddMethod_DelimitterIsNewlineReturnTheSum(string numbers, int realSum)
        {
            //arrange

            //act
            sum = calc.Add(numbers);

            //assert
            Assert.AreEqual(realSum, sum);
        }


        [TestMethod]
        public void AddMethod_SupportDiffDelimitersReturnSum0()
        {
            //arrange


            //act
            sum = calc.Add("//;\n1;2");

            //assert
            Assert.AreEqual(3, sum);
        }


    }


}
