﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingScoring
{
    public class BowlingScoringEngine
    {
        //score the bowling game. The input is an array of integers 
        //signifying the rolls
        public int Score(int[] rolls)
        {
            int score = 0;
            int frameScore = 0;
            int frameRoll = 1;

            //add up each role in the rolls list
            for (int i = 0; i < rolls.Length; i++)
            {



                //keep track of the score in each frame
                //and only add the framescore to total score at the end of the frame.
                frameScore += rolls[i];



                //if strike
                if (frameRoll == 1 && frameScore == 10)
                {
                    //add to score 10 plus the next two rolls
                    if (score < 300)
                    {
                        score += 10 + rolls[i + 1] + rolls[i + 2];
                        frameScore = 0;
                    }
                }
                //if spare
                else if (frameRoll == 2 && frameScore == 10)
                {
                    score += 10 + rolls[i + 1];
                    frameRoll = 1;
                    frameScore = 0;
                }
                //if end of second frame
                else if (frameRoll == 2)
                {
                    score += frameScore;
                    frameRoll = 1;
                    frameScore = 0;
                }
                //if 1st roll of frame just continue to next roll
                else frameRoll++;

                
                
                
                
                
            }
            return score;
        }
    }
}
