﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BowlingScoring;

namespace BowlingScoringTests
{
    [TestClass]
    public class BowlingScoringEngineTests
    {
        [TestMethod]
        public void GutterGamesScoreIs0()
        {
            //arrange
            BowlingScoringEngine scoreEngine = new BowlingScoringEngine();
            int[] rolls = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            //act
            int result = scoreEngine.Score(rolls);

            //assert
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void AllOnesGamesScoreIs20()
        {
            //arrange
            BowlingScoringEngine scoreEngine = new BowlingScoringEngine();
            int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

            //act
            int result = scoreEngine.Score(rolls);

            //assert
            Assert.AreEqual(20, result);
        }

        [TestMethod]
        public void SpareInFirstFrame_3InNextFrame_0RestOfFrames_ScoreIs16()
        {
            //arrange
            BowlingScoringEngine scoreEngine = new BowlingScoringEngine();
            int[] rolls = { 2, 8, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            //act
            int result = scoreEngine.Score(rolls);

            //assert
            Assert.AreEqual(16, result);
        }


        [TestMethod]
        public void StrikeInFirstFrame_3and4NextFrame_0RestOfFrames_ScoreIs24()
        {
            //arrange
            BowlingScoringEngine scoreEngine = new BowlingScoringEngine();
            int[] rolls = { 10, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            //act
            int result = scoreEngine.Score(rolls);

            //assert
            Assert.AreEqual(24, result);
        }


        [TestMethod]
        public void PerfectGame_ScoreIs300()
        {
            //arrange
            BowlingScoringEngine scoreEngine = new BowlingScoringEngine();
            int[] rolls = { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,10 };

            //act
            int result = scoreEngine.Score(rolls);

            //assert
            Assert.AreEqual(300, result);
        }


    }
}
